CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Line Awesome consists of ~1380 flat line icons that offer complete
coverage of the main Font Awesome icon set. This icon-font is based
off of the Icons8 Windows 10 style, which consists of over 
4,000 icons, so be sure to check that out if you need more specific icons.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/line_awesome

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/line_awesome


REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------

No recommended modules.

INSTALLATION
------------

The module is using CDNs to give flexibility to the installation unless you
want to add libraries.

See options below in the optional installation section:

OPTION 1 - DOWNLOAD REQUIRED LIBRARIES (OPTIONAL INSTALLATION)
--------------------------------------------------------------

1. Download Icons8 Line Awesome library 
(https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/line-awesome-1.3.0.zip)
2. [Drupal 8-9] Extract the library under libraries/line_awesome
3. Download and enable the module

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.

OPTION 2 - COMPOSER (OPTIONAL INSTALLATION)
-------------------------------------------

1. Copy the following into your project's composer.json file.
```json
"repositories": [
  {
    "type": "package",
    "package": {
      "name": "icons8/line_awesome",
      "version": "1.3.0",
      "type": "drupal-library",
      "dist": {
        "url": "https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/line-awesome-1.3.0.zip",
        "type": "zip"
      }
    }
  }
]
```

2. Ensure you have following mapping inside your composer.json.
```json
"extra": {
  "installer-paths": {
    "web/libraries/{$name}": ["type:drupal-library"]
  }
}
```

3. Run following command to download required library.
```php
composer require icons8/line_awesome
```
 
4. Enable the Line Awesome Icons

CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * David Galeano (gxleano) - https://www.drupal.org/u/gxleano
